package binspot;

public interface IOperand {
  public StringBuilder asString (StringBuilder buf);
}
