package binspot.gdsl;

import gdsl.rreil.IRReilCollection;
import gdsl.rreil.statement.IStatement;
import gdsl.translator.Translator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;

import rreil.disassembler.Instruction;
import rreil.disassembler.OperandTree;
import rreil.disassembler.translators.common.TranslationException;
import rreil.gdsl.BindeadGdslRReilBuilder;
import rreil.gdsl.StatementCollection;
import rreil.gdsl.builder.StatementCollectionBuilder;
import rreil.lang.RReil;
import rreil.lang.RReilAddr;
import rreil.lang.lowlevel.LowLevelRReil;
import rreil.lang.lowlevel.RReilHighLevelToLowLevelWrapper;

public class GdslInstruction extends Instruction {
  private final gdsl.decoder.NativeInstruction gdslInsn;

  public GdslInstruction (long address, byte[] opcode, String mnemonic, gdsl.decoder.NativeInstruction gdslInsn,
      List<OperandTree> opnds) {
    super(address, opcode, mnemonic.toLowerCase(), opnds);
    this.gdslInsn = gdslInsn;
  }

  public GdslInstruction (RReilAddr address, byte[] opcode, String mnemonic, gdsl.decoder.NativeInstruction gdslInsn,
      OperandTree... opnds) {
    super(address, opcode, mnemonic.toLowerCase(), opnds);
    this.gdslInsn = gdslInsn;
  }

  public GdslInstruction (RReilAddr address, byte[] opcode, String mnemonic, gdsl.decoder.NativeInstruction gdslInsn,
      List<OperandTree> opnds) {
    super(address, opcode, mnemonic.toLowerCase(), opnds);
    this.gdslInsn = gdslInsn;
  }

  @Override public List<LowLevelRReil> toRReilInstructions () throws TranslationException {
    List<LowLevelRReil> rreil = new ArrayList<LowLevelRReil>();

    Translator t = new Translator(gdslInsn.getGdsl(), new BindeadGdslRReilBuilder(address()));
    IRReilCollection<IStatement> coll = t.translate(gdslInsn);
    StatementCollection statements = ((StatementCollectionBuilder) coll).build().getResult();

    SortedMap<RReilAddr, RReil> instructions = statements.getInstructions();
    for (Iterator<RReil> it = instructions.values().iterator(); it.hasNext();) {
      RReil next = it.next();
      rreil.add(new RReilHighLevelToLowLevelWrapper(next));
    }
    return rreil;
  }

  /**
   * {@inheritDoc}
   */
  @Override public String toString () {
    StringBuilder builder = new StringBuilder();
    builder.append(String.format("%-6s", gdslInsn.mnemonic()));
    builder.append(" ");
    for (int i = 0; i < gdslInsn.operands(); i++) {
      builder.append(gdslInsn.operandToString(i));
      if (i < gdslInsn.operands() - 1)
        builder.append(", ");
    }
    return prettyPrintFixup(builder.toString());
  }

  private static String prettyPrintFixup (String instruction) {
    return instruction.toLowerCase().replaceAll("dword ptr", "DWORD PTR");
  }
}
