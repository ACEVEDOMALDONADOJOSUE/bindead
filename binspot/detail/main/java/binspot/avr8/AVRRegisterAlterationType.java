package binspot.avr8;

public enum AVRRegisterAlterationType {
  None, PreDecrement, PostIncrement
}
