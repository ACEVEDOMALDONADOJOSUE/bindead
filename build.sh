#!/bin/sh
## Requires Apache Maven installed and an internet connection as Maven will download lots of things the first time!
set -e #immediately exit on errors

# install thrid-party libraries. These are bound to the clean phase as a workaround for 
# a non-existing other phase to install dependencies before checking for them
mvn clean
# now do the normal install but skip tests to be faster
mvn install -DskipTests
# build the standalone analyzer and copy it to toplevel directory
cd bindead
mvn clean compile assembly:single
cp target/bindead-*.jar ../bindead.jar