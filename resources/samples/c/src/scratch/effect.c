
#include "test.h"

void effectFn (int*, size_t, int) __attribute__((noinline));
void effectFn (int* a, size_t sz, int v)
{
    int i;
    for ( i = 0; i < sz; i++) {
        a[i] = v;
    }
}

int testEffectFn ()
{
    int c[4];
    int b[4];

    // RRR
    effectFn (c, 4, 19);
    effectFn (b, 5, 23);

    return (c[0] + b[0]);
}
