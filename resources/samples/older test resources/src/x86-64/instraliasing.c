//@summarystart
// Example which illustrates McVeto's ability to deal with
// instruction aliasing.
//@summaryend
//#include "../include/reach.h"

// This example is slightly modified to avoid the usage of a global data segment for the variable to be modified
// instead it uses a reference to modify a variable on the stack of the caller

int foo(int *a) { // In the object file, the address of foo is 0
  *a += 4;
  return 1;
}

int main() {
  int n = 3;

  asm (
    "movq $2, %rax\n"
    "lea -4(%rbp), %rdi\n" // load address of n; assumes the fixed offset from the base pointer, works only with GCC -O0
    "L1: movq $0x5bd0ff53, %rcx\n" // This instruction becomes "push rbx; call rax; pop rbx" when disassembled 3 bytes later
    "cmp $1, %rax\n"
    "jz  L2\n"
    "movq $foo, %rax\n"
    "movq $L1, %rdx\n"
    "addq $3, %rdx\n"
    "jmpq *%rdx\n"
    "L2: xor %rdx, %rdx\n");

  return (n == 7);
}

