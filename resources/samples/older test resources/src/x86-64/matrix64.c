
#include <stdlib.h>

int array_int (int*, size_t) __attribute__ ((noinline));
int array_int (int* a, size_t sz)
{
  int i, r;

  r = 0;
  for ( i = 1; i < sz; i++) {
    r += a [i] - a [i-1];
  }

  return (r);
}

int main (int argc, char** argv)
{
  long m[100][100];
  long n = 1;

  long i,j;
  for (i = 0; i < 100; i++) {
    for (j = 0; j < 100; j++) {
       n += m[i][j];
    }
  }

  return n;
}
