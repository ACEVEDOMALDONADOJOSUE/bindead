#include "../include/reach.h"

// @summarystart
/* Functions main() in client.c and functions server_setup() and */
/* server_client_connect() in server.c are vulnerable. All three */
/* functions use snprintf() to copy the $HOME environment variable into a */
/* fixed-size buffer (safe); however, the contents of this buffer are */
/* later copied into a smaller buffer using strcpy(). */

/* Algorithmically, this overflow is very simple, and isn't */
/* string-content-based. The difficult parts are the technical bits -- */
/* modelling snprintf, structures, and unions. */
// @summaryend

/* Size of buffer being overflowed. 
 * Ensure that SUN_PATH_SZ - 1 is non-negative */
#define SUN_PATH_SZ BASE_SZ + 1/* originally 108 */

/* Size of input buffer. */
#define FILENAME_SZ SUN_PATH_SZ + 2  /* originally 1024 */

struct sockaddr_un
{
  char sun_path[SUN_PATH_SZ];         /* Path name.  */
  char canary;
};

int main ()
{
    struct sockaddr_un serv_adr;
    char               filename [FILENAME_SZ];
    serv_adr.canary = BRIGHT_YELLOW;

    /* server filename */
    filename[FILENAME_SZ-1] = EOS;
    
    /* initialize the server address structure */
    /* BAD */
    r_strcpy (serv_adr.sun_path, filename);
    if(serv_adr.canary != BRIGHT_YELLOW)
    {
        REACHABLE();
    }

    return 0;
}
